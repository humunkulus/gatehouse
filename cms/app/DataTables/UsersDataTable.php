<?php

namespace CMS\DataTables;

use CMS\User;
use Yajra\Datatables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'path.to.action.view');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    // public function query()
    // {
    //     $query = User::query();

    //     return $this->applyScopes($query);
    // }

    // /**
    //  * Optional method if you want to use html builder.
    //  *
    //  * @return \Yajra\Datatables\Html\Builder
    //  */
    // public function html()
    // {
    //     return $this->builder()
    //                 ->columns($this->getColumns())
    //                 ->ajax('')
    //                 ->addAction(['width' => '80px'])
    //                 ->parameters($this->getBuilderParameters());
    // }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            // add your columns
            'created_at',
            'updated_at',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'usersdatatable_' . time();
    }


    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->make(true);
    }

    public function query()
    {
        $users = User::select();

        return $this->applyScopes($users);
    }

    public function html()
    {
        return $this->builder()
            ->columns([
                'id',
                'name',
                'email',
                'created_at',
                'updated_at',
            ])
            ->parameters([
                'dom' => 'Bfrtip',
                'buttons' => ['csv', 'excel', 'pdf', 'print', 'reset', 'reload'],
            ]);
    }

}
