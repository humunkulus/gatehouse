<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'invoice_detail';

    public $timestamps = false;
    /**
     * Fields.
     *
     * @var array
     */
    protected $fillable = ['invoice_detail_id', 'item_no' ,'description', 'quantity', 'unit_price', 'tax_rate', 'amount'];
}
