<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;

class InvoiceStatus extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'invoice_status';

    public $timestamps = false;
    /**
     * Fields.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];
}

