<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * The table associated with the model
    */

    protected $table = "client";

    /**
     * Fields.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

}
