<?php
namespace CMS\Http\Controllers;

use Illuminate\Support\Facades\DB;
use CMS\Http\Controllers\Controller;
use CMS\Client;

class ClientController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getClient()
    {
        $client_request = Client::all();
        return view('invoice')->with('client_request', $client_request);
    }
}


