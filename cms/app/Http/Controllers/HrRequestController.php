<?php

namespace CMS\Http\Controllers;

use Illuminate\Support\Facades\DB;
use CMS\Http\Controllers\Controller;
use CMS\InvoiceStatus;

class HrRequestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hr_request = InvoiceStatus::all();
        return view('view-hr-requests')->with('hr_request', $hr_request);
    }
}


