<?php
namespace CMS\Http\Controllers;

use Validator; 
use Datatables;
use CMS\Invoice;
use CMS\InvoiceDetail;
use CMS\Data; 
use CMS\Client;
use CMS\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 
use Illuminate\Http\Response; 


use Illuminate\Support\Facades\Input;

class InvoiceController extends Controller
{



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoice_request = Invoice::all();
        return view('invoice')->with('invoice_request', $invoice_request);
    }

    public function validator(array $data, array $filter)
    {    
        return Validator::make($data, $filter);
    }

    public function create(Request $request)
    {
        $values  = [];
        $filter  = [];
        $invCnt  = 0;
        $taxType = "";

        // foreach ($request->all() as $key => $value) 
        // {
            foreach ($request->all() as $key2 => $value2) 
            {
                if(is_array($value2))
                {
                    $index  = 1;
                    $invCnt = count($value2);
                    foreach ($value2 as $key3 => $value3) 
                    {
                        
                        $ind = $key2.$index;
                        $values[$ind] = $value3;
                        $index++;

                        if($key2 == "description" || $key2 == "item")
                        {
                            $filter[$ind] = "required|string";  
                        }
                        elseif($key2 == "price" || $key2 == "qty")
                        {
                            $filter[$ind] = "required|numeric"; 
                        }
                        elseif($key2 == "tax-rate")
                        {
                            $filter[$ind] = "";    
                        }
                        elseif($key2 == "discount")
                        {
                            $filter[$ind] = "nullable|numeric"; 
                        }
                        else{
                            $filter[$ind] = "nullable";     
                        }
                    }
                }
                else
                {
                    if($key2 == "client")
                    {
                        $filter[$key2] = "required|numeric";    
                    }
                    elseif($key2 == "date" || $key2 == "duedate")
                    {
                        $filter[$key2] = "required|dateformat:Y-m-d";  
                    }
                    elseif($key2 == "tax-rate" || $key2 == "note" || $key2 == "tax" || $key2 == "tax")
                    {
                        $filter[$key2] = "required";  

                        if($key2 == "tax")
                        {   
                            $taxType = $value2;
                        }
                    }
                    else{
                        $filter[$key2] = "nullable";     
                    }

                    $values[$key2] = $value2;
                }
            }
        //}

        $invAmount = 0;
        $rowAmount = [];
        $subAll    = 0;
        $gst       = 0;

        // calculate invoice amount
        for($i=1; $i<=$invCnt; $i++)
        {
            $priceInd  = "price".$i;
            $qtyIndex  = "qty".$i;
            $discount  = "discount".$i;
            $taxIndex  = "tax-rate".$i;
            $subtotal  = 0;
            
            $itemPrice = $values[$priceInd] * $values[$qtyIndex];
            $subtotal  = $itemPrice;

            if($discount != ""){
                $percent = $values[$discount]/100;
                $itemPrice = ($itemPrice - ($itemPrice * $percent));
                $subtotal  = $itemPrice;
            }
            if($taxIndex != ""){
                if($taxType == "exclusive" && $values[$taxIndex] == "GST on Income"){
                    $gst      += $itemPrice * 0.1;
                    $subtotal  = $itemPrice;
                    $itemPrice = ($itemPrice + ($itemPrice * 0.1));     
                }
                elseif($taxType == "inclusive" && $values[$taxIndex] == "GST on Income"){
                    $gst       += (($itemPrice * 0.1)/1.1);
                    $subtotal   = $itemPrice;
                    $itemPrice  = $itemPrice; 
                }
                else{
                    $itemPrice = $itemPrice;
                    $subtotal   = $itemPrice;
                }
            }

            $rowAmount[$i] =$itemPrice;
            $invAmount += $itemPrice; 
            $subAll += $subtotal; 

        }

        // $number = ["row" => $rowAmount, "total" => $invAmount, "gst" => $gst, "subtotal" => $subtotal, $taxType => $taxIndex];
        // return  json_encode($number);

        // validate user input
        $data = $this->validator($values, $filter)->validate();        

        if ($data == null)
        {
            // insert invoice header
            $resultID = DB::table('invoice')->insertGetId([
                            'invoice_no'     => "",
                            'client_id'      => $request['client'],
                            'project_id'     => 1,
                            'freelancer_id'  => 1,
                            'invoice_date'   => $request['date'],
                            'due_date'       => $request['duedate'],
                            'reference_no'   => "",
                            'invoice_amount' => $invAmount,
                            'notes'          => $request['note'],
                            'status'         => 1,
                            'tax_type'       => $request['tax'],
                            'sub_total'      => $subAll,
                            'gst'            => $gst
                        ]);

            $invoicenum= "INV".str_pad($resultID, 8, '0', STR_PAD_LEFT);
            $reference = "REF".str_pad($resultID, 8, '0', STR_PAD_LEFT);

            // update header invoice number and reference number
            $results = DB::table('invoice')
                        ->where('invoice_id', $resultID)
                        ->update(['invoice_no' => $invoicenum, 'reference_no' => $reference]);            

            // loop insert invoice detail                        
            for($i=1; $i<=$invCnt; $i++)
            {   
                $item        = "item".$i;
                $description = "description".$i;
                $quantity    = "qty".$i;
                $discount    = "discount".$i;
                $unit_price  = "price".$i;
                $tax_rate    = "tax-rate".$i;
                $amount      = "amount".$i;

                DB::table('invoice_detail')->insert([
                    'item'        => $values[$item],
                    'description' => $values[$description],
                    'quantity'    => $values[$quantity],
                    'discount'    => $values[$discount],
                    'unit_price'  => $values[$unit_price],
                    'tax_rate'    => $values[$tax_rate],
                    'amount'      => $rowAmount[$i],
                    'invoice_id'  => $resultID
                ]);
            }

            $message = array('message'        => 'Invoice was successfully saved.', 
                             'status'         => 1,
                             'client_request' => Client::all(),
                             'invoice_id'     => $invoicenum,
                             'reference_no'   => $reference);    

            return json_encode($message);

        }
        else{
            return view('invoice')->withFlashErrors($data->errors);
        }

    }

    public function storeInvoice(array $data)
    {
        
        return $data;
    }

    public function showInvoiceForm()
    {
        $client_request = Client::all();
        return view('invoice')->with('client_request', $client_request);
    }

    public function showData(Request $request, $id)
    {
        $invoice_request = DB::table('invoice')->where('invoice_id', '=', $id)->get();
        return view('invoice-update')->with('invoice_request', $invoice_request);

    }

    public function viewInvoice(Request $data)
    {
        $invoice = DB::table('invoice')
                ->select('invoice_no as inv_no',  
                         'name as client',  
                         'invoice_date as date',  
                         'due_date as due',  
                         'invoice_amount as amount',  
                         'status',
                         'gst',
                         'sub_total as subtotal')
                ->join('client', 'invoice.client_id', '=', 'client.client_id')
                ->where('invoice_id', '=', $data['id'])
                ->get();

        $details = DB::table('invoice_detail')
                ->select('description', 'quantity', 'unit_price', 'discount', 'tax_rate', 'amount')
                ->join('invoice', 'invoice_detail.invoice_id', '=', 'invoice.invoice_id')
                ->where('invoice_detail.invoice_id', '=', $data['id'])
                ->get();

        $responseText = ["header" => $invoice, "details" => $details];            

        return $responseText;
    }

    public function act(Request $data)
    {
        return $data['action'];

    }

    public function deleteInvoice(Request $data)
    {
        $delete = DB::table('invoice')->where('invoice_id', '=', $data['id'])->delete();
        return $delete;
        //return view('invoice-list')->with('response', $delete);

    }

    public function updateInvoiceView($data){
        //$data    = (int)$data;
        $header  = DB::table('invoice')->where('invoice_id', '=', $data)->get();
        $details = DB::table('invoice_detail')->where('invoice_id', '=', $data)->get();
        $client  = Client::all();

        $response = ['header' => $header, 'details' => $details, 'client' => $client];

        return view('invoice-update')->with('response', $response);
    }


}


