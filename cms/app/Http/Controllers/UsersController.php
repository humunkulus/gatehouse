<?php

namespace CMS\Http\Controllers;

use CMS\DataTables\UsersDataTable;
use CMS\Http\Requests;

class UsersController extends Controller
{
    public function index(UsersDataTable $dataTable)
    {
        return $dataTable->render('users');
    }
}