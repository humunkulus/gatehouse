<?php
namespace CMS\Http\Controllers;

use CMS\Invoice;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;



class DatatablesController extends Controller
{
    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        //return view('invoice-list');
        $invoice_request = Invoice::all();
        return view('invoice-list')->with('invoice_request', $invoice_request);
    }

    /**
     * Process dataTable ajax response.
     *
     * @param \Yajra\Datatables\Datatables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
     
    public function data(Datatables $datatables)
    {
    	//if (request()->ajax()) {
	        return $datatables->eloquent(Invoice::select('invoice_date', 'due_date', 'reference_no', 'notes', 'invoice_amount'))
	                          ->addColumn('action', 'eloquent.tables.invoice-action')
	                          ->rawColumns([6])
	                          ->escapeColumns([])
	                          ->make(true);



		//}

		// return view('invoice-list');	                          

        // $invoices = Invoice::select(['invoice_date', 'due_date', 'reference_no', 'notes', 'invoice_amount']);

        // return Datatables::of($invoices)->make();
    	//return Datatables::of(Invoice::all())->make(true);


    }
}


