<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use CMS\Http\Controllers\Controller;

class InvoiceStatusController extends Controller
{
    /**
     * Show a list of all of the invoice status.
     *
     * @return Response
     */
    // public function fetchStatus()
    // {
    //     $status = DB::table('invoice_status')->select('name')->('inv_status_id', '=', 1 )where()->get();

    //     return view('child', ['invoice_status' => $status]);
    // }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hr_request = HrRequest::all();
        return view('view-requests')->with('hr_request', $hr_request);
    }
    
}