<?php

namespace CMS\Http\Controllers;

use Illuminate\Http\Request;
use CMS\Http\Controllers\Controller;


class InvoicePostsController extends Controller
{
	/*
	*
	* Show Invoice Form
	* @return response
	*/

	public function create()
	{
		return view('posts.create');

	}


	/*
	* Store the newly created invoice
	*/
	public function store(Request $request)
	{

		$this->validate($request, [
			'client'     => 'required',
			'date'       => 'required',
			'duedate'    => 'required',
			'invoicenum' => 'required',
			'reference'  => 'required',
			'tax'        => 'required',
			'amount'     => 'required|digit',
			'note'       => 'required'
		]);
	}


}