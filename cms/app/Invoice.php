<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'invoice';

    public $timestamps = false;
    /**
     * Fields.
     *
     * @var array
     */
    protected $fillable = ['invoice_no', 'client_id', 'freelancer_id', 'reference_no', 'project_id', 'invoice_date', 'due_date', 'invoice_amount', 'notes'];
}



