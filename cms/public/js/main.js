
$(document).ready(function(){

	
	$('body').on("click", ".previewInvoice", function (e) {
		var modal = $(this).attr("id");;
		var CSRF_TOKEN = $('meta[name="_token"]').attr('content');

		console.log(modal);

		$.ajax({
            type: "POST",
            url: 'invoice-list',
            data: "id="+modal,
            beforeSend: function (xhr) {
		        return xhr.setRequestHeader('X-CSRF-TOKEN', CSRF_TOKEN);
		    },
            success: function( response ) {
                console.log(response);

				var header  = response['header'][0];
				var details = response['details'];
				var status  = response['header'][0].status;
				var title   = "";

                if(status = 1){ title = "Draft"; }
                else if(status = 2){ title = "For Approval";}
                else if(status = 3){ title = "For Review"; }
                else if(status = 4){ title = "For Payment"; }
                else{ title = "Paid"; }

                $("#modalLabel").html(title+" Invoice");
                $("#to").html(header.client);
				$("#issued").html(header.date);
				$("#due").html(header.due);
				$("#subtotal-row").html(header.subtotal);
				$("#gst-row").html(header.gst);
				$("#amount-row").html(header.amount);
				
				// populate invoice details
				var data    = $(".multi");
				var display = "";
				var counter = 1;

				$("#preview-table tbody").empty();

				$.each(details, function( key, val ) 
				{		
					display += "<tr>";
				
					$.each(val, function( key1, val1 ) 
					{				

						console.log(key1 +" "+ val1);
						val1 = (val1 == null) ? '' :val1;
						display += 	"<td>"+val1+"</td>";
					});
					display += "</tr>";
				})	

				display += "</tr>";

				$("#preview-table tbody").append(display);

            }
        });

	});

	// preview modal
	$('#preview').on("show.bs.modal", function (e) {
		var client  = $("#client option:selected").text();
		var date    = $("#date").val();
		var duedate = $("#duedate").val();
		var data    = $(".multi");
		var display = "<tr>";
		var counter = 1;

		$("#to").html(client);
		$("#issued").html(date);
		$("#due").html(duedate);
		$("#subtotal-row").html();

		$("#preview-table tbody").empty();

		$.each(data, function( key, val ) 
		{		
			if(counter % 6 === 0)
			{
				display += 	"<td>"+val.value+"</td></tr><tr>";
			}
			else
			{
				display += 	"<td>"+val.value+"</td>";
			}
			
			counter++;
		})	
		display += "</tr>";

		$("#preview-table tbody").append(display);

		display = "";

    });

	// calculate discount
	/*$("#invoice-detail").on("change", ".discount", function(e){
			var percent   = parseInt($(this).val()) / 100;
			var quantity   = $(this).closest("tr").find(".qty").val();
			var price      = $(this).closest("tr").find(".price").val();
			

			if(price != "" && quantity != ""){
				price = parseInt(price);
				quantity = parseInt(quantity);
				var total = quantity * price;
				var discounted = (total - (total * percent));	
				$(this).closest("tr").find(".invoice-amount").val(discounted);	
			}
			else{
				$(this).val("");
			}
					
			console.log(percent +"-"+ price +"-"+ quantity +"-"+ total +"-"+ discounted);

	});*/


	// show/hide taxrate options
	$("#invoice-detail").on("click", ".taxrate", function(e){
		$(this).closest("td").find(".tax-class").show();
		e.stopPropagation();
	});
	$("#invoice-detail").on("click", ".tax-class div", function(e){
		var cont = $(this).html();
		console.log(cont);
		$(this).closest("td").find(".taxrate").val(cont);
		$(".tax-class").hide();
		calcAmount();
	});
	$("body").click(function(e) {
		$(".tax-class").hide();
	});


	// delete invoice detail row
	$("#invoice-detail").on("click", '.deleteRow', function(e){
		if($('.deleteRow').length > 1){
			$(this).closest("tr").remove();
		}
	});

	// add new invoice row item
	$(".addRow").click(function(){
		$(this).closest("tr").prev("tr").clone().insertBefore(".addRowBtn");
		clearFields();
	});

	// calculate total amount
	/*$("#invoice-detail").on("change", '.invoice-amount', function(e){
		var total = 0;	
		var amount = $(".invoice-amount");
		$.each(amount, function( key, val ) {
  			total += parseInt($(".invoice-amount")[key].value);
		});

		total = parseFloat(total).toFixed(2);

		$("#sub-total").html(total);
		$("#total").html(total);
	});*/

	// compute invoice amount
	$("body").on("change", '.compute', function(e){
		calcAmount();
	});

	function clearFields(){
		var ind = $("input[name^=amount]").length -1;
		$("input[name^=item]").eq(ind).val("");
		$("input[name^=description]").eq(ind).val("");
		$("input[name^=qty]").eq(ind).val("");
		$("input[name^=price]").eq(ind).val("");
		$("input[name^=disc]").eq(ind).val("");
		$("input[name^=tax-rate]").eq(ind).val("");
		$("input[name^=amount]").eq(ind).val("");
	}

 	function calcAmount(){
		var total    = 0	
		var qty      = $(".qty");
		var price    = $(".price");
		var discount = $(".discount");
		var taxrate  = $(".taxrate");
		var taxType  = $("#tax").val();
		var len      = qty.length;
		var qtyVal   = 0;
		var priceVal = 0;
		var percent  = 0;
		var subAll   = 0;
		var gst      = 0;

		for(var i=0; i<len; i++){
			var itemAmount = 0;
			var subtotal = 0;
			

			qtyVal      = qty[i].value;
			priceVal    = price[i].value;
			discountVal = discount[i].value;
			taxrateVal  = taxrate[i].value;
			percent     = (discountVal/100);
				
			console.log("precent-"+percent +" value-"+discountVal);

			console.log("tax" + taxrateVal);

			if(qtyVal != "" && priceVal !="")
			{	
				itemAmount = parseInt(qtyVal) * parseInt(priceVal);
				subtotal   = itemAmount;

				if(discountVal !="")
				{						
					itemAmount  = (itemAmount - (itemAmount * percent));
					subtotal   = itemAmount;
				}
				if(taxrateVal != "")
				{
					//console.log("taxrate = " + itemAmount);

					if(taxType == "exclusive" && taxrateVal == "GST on Income"){							
						gst         += itemAmount * 0.1;
						subtotal    = itemAmount;
						itemAmount  = (itemAmount + (itemAmount * 0.1));    
					}
					else if(taxType == "inclusive" && taxrateVal == "GST on Income"){
						// console.log("10% - " + (itemAmount * 0.1)); 
						// itemAmount = (itemAmount - ((itemAmount * 0.1)/1.1));	

						gst        += ((itemAmount * 0.1)/1.1);
						subtotal    = itemAmount;
						itemAmount  = itemAmount; 
					}
					else{
						itemAmount = itemAmount;
						subtotal   = itemAmount;
					}
				}
				
				subAll += subtotal;
				total += itemAmount;
				if(len > 1){
					$("input[name^=amount]").eq(i).val(parseFloat(itemAmount).toFixed(2));
				}
				else{
					$(".invoice-amount").val(parseFloat(itemAmount).toFixed(2));
				}
			}
		}

		total = parseFloat(total).toFixed(2);
		subAll = parseFloat(subAll).toFixed(2);
		gst = parseFloat(gst).toFixed(2);
		
		$("#gst").html(gst);
		$("#sub-total").html(subAll);
		$("#total").html(total);
 	}


	// process the form
	// create invoice
    $('#formSubmit').click(function(event) {

        var formData = $("form").serialize();

        $.ajax({
		    type: 'post',           // POST Request
		    url: 'invoice',            // Url of the Route (in this case user/save not only save)
		    data: formData,         // Serialized Data
		    dataType: 'json',       // Data Type of the Transmit
		    beforeSend: function (xhr) {
		        // Function needed from Laravel because of the CSRF Middleware
		        var token = $('meta[name="csrf_token"]').attr('content');

		        if (token) {
		            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
		        }
		    },
		    success: function (data) {		    

		        if(data.status == 1)
		        {
		        	$("#inv_no").html(data.invoice_id);
		        	$("#inv_ref").html(data.reference_no);	
		        	$(".alert").removeClass('alert-danger').addClass('alert-success').html(data.message).show();
		        }		    			        		        

		    },
		    error: function (data) {
		        
		        if(data.status!= 200){		        
			        var errorList = $.parseJSON(data.responseText);
			        var errorMsg  = "";

					$.each(errorList, function(k, v) {
						errorMsg += v + "<br />";
						
					});
					

					$("#formError").html(errorMsg);
					$(".alert").show();
				}
				else{
					console.log(data);
					$(".alert").hide();
				}
		    },

		});
        event.preventDefault();
    });


    // datatable initialization 
	//setTimeout(function(){
	 var table = $('#invoice-table').DataTable( {
        		"columnDefs": [ {
	            	"searchable": false,
	            	"orderable": false,
	            	"targets": "no-sort"
			        } ],
			        "order": [[ 1, 'asc' ]]
			    } );

	table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
	// }, 500);

	// delete invoice
	$('body').on("click", ".deleteInvoice", function (e) {

		$(this).closest("tr").addClass("selectedRow");

		var id = $(this).attr("id");
		$(".deleteThis").attr("id", id);
	});
	$(".cancelDelete").click(function(){
		$("table tr").removeClass("selectedRow");
		console.log("delete daw nbi");
	});

	$(".deleteThis").click(function(){
		var id = $(this).attr("id");
		deleteInvoice(id);
	});


	function deleteInvoice(id){
		var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
		$.ajax({
	        type: "POST",
	        url: 'invoice-list/'+id,
	        data: "id="+id,
	        beforeSend: function (xhr) {
		        return xhr.setRequestHeader('X-CSRF-TOKEN', CSRF_TOKEN);
		    },
	        success: function( response ) {
	        	console.log(response);
	        	if(response['response']==1){
	        		table.row('.selectedRow').remove().draw( false );
	        	}
	        	$('#delete').modal('toggle'); 
    			
	        }
	    });    
	}



	// $('#invoice-table').DataTable({
 //        "processing":true,
 //        "serverSide":true,
 //        "stateSave":true,
 //        "ajax":{
 //                 "url": "{{ url('invoice-list') }}",
 //                 "dataType": "json",
 //                 "type": "POST",
 //                 "data":{ _token: "{{csrf_token()}}"}
 //               },
 //        "columns": 
 //        [
 //            { "data": "invoice_date" },
 //            { "data": "due_date" },
 //            { "data": "reference_no" },
 //            { "data": "notes" },
 //            { "data": "invoice_amount" },
 //            { "data": "action" }
 //        ]
 //    });

        



});