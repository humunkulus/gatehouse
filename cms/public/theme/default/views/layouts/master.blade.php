<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="_token" content="{{ csrf_token() }}" />
        <title>Alifery - @yield('title')</title>
		
    	<link rel="stylesheet" href="/css/app.css" />	
    	<link rel="stylesheet" href="/css/styles.css" />	
        <link rel="stylesheet" href="/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    	
        <script src="/js/jquery-3.2.1.min.js"></script>
    	<script src="/js/app.js"></script>	
        <script src="/js/main.js"></script>  	
        <script src="/js/jquery.dataTables.min.js"></script>
    </head>
    <body>

  		<nav class="navbar navbar-inverse bg-inverse navbar-toggleable-md">
        <div class="container">
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleContainer" aria-controls="navbarsExampleContainer" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <a class="navbar-brand" href="#"><img src="{!! url('/images/AliferyLogoFinal-Blue.png') !!}" alt="Alifery"></a>
        </div>
      </nav>

        @section('sidebar')
            <!-- This is the master sidebar. -->
        @show

        <div class="container">
            @yield('content')
        </div>
        <!-- <script src="/js/jquery-ui.min.js"></script>    --> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
        <script>
          $(function() {
            $( ".datepicker" ).datepicker({ format: 'yyyy-mm-dd' });
          });
         </script>
    </body>
</html>