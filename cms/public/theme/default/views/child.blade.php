<!-- Stored in resources/views/child.blade.php -->

@extends('layouts.master')

@section('title', 'Invoice')

@section('sidebar')
    @parent

    <!-- <p>This is appended to the master sidebar.</p> -->
@endsection

@section('content')
    

	<form>

		<h2>Invoice</h2>


    <!-- <div class="input-group date">
	  <input class="form-control" type="text"><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
	</div> -->


	<div class="form-group">

		<table class="table table-responsive">
		  <thead class="thead-inverse">
		    <tr>
		      <th>To</th>
		      <th>Date</th>
		      <th>Due Date</th>
		      <th>Invoice #</th>
		      <th>Reference</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td>
		      	<div class="form-group">
    				<select class="form-control" id="exampleSelect1">
				      <option value="Open Contract 1">Open Contract 1</option>
				      <option value="Open Contract 2">Open Contract 2</option>
				      <option value="Open Contract 3">Open Contract 3</option>
				      <option value="Open Contract 4">Open Contract 4</option>
				      <option value="Open Contract 5">Open Contract 5</option>
				    </select>
  				</div>
  			  </td>
		      <td>
		      	<div class="form-group">
    				<input type="text" class="form-control" id="fieldID" placeholder="Field name">
  				</div>		      	
		      </td>
		      <td>
		      	<div class="form-group">
    				<input type="text" class="form-control" id="fieldID" placeholder="Field name">
  				</div>
		      </td>
		      <td>
		      	<div class="form-group">
    				<input type="text" class="form-control" id="fieldID" placeholder="Field name">
  				</div>
		      </td>
		      <td>
		      	<div class="form-group">
    				<input type="text" class="form-control" id="fieldID" placeholder="Field name">
  				</div>
		      </td>
		  </tbody>
		</table>

		<div class="form-group">
			<label for="note">Amounts are</label>
		    <select class="form-control" id="exampleSelect1">
		      <option value="">Tax Exclusive</option>
		    </select>
		  </div>


		<table class="table table-responsive">
		  <thead class="thead-inverse">
		    <tr>
		      <th>#</th>
		      <th>Item</th>
		      <th>Description</th>
		      <th>Qty</th>
		      <th>Unit Price</th>
		      <th>Account</th>
		      <th>Tax Rate</th>
		      <th>Amount</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <th scope="row">1</th>
		      <td>
		      	<div class="form-group">
    				<input type="text" class="form-control" id="fieldID" placeholder="Field name">
  				</div>
  			  </td>
		      <td>
		      	<div class="form-group">
    				<input type="text" class="form-control" id="fieldID" placeholder="Field name">
  				</div>		      	
		      </td>
		      <td>
		      	<div class="form-group">
    				<input type="text" class="form-control" id="fieldID" placeholder="Field name">
  				</div>
		      </td>
		      <td>
		      	<div class="form-group">
    				<input type="text" class="form-control" id="fieldID" placeholder="Field name">
  				</div>
		      </td>
		      <td>
		      	<div class="form-group">
				    <select class="form-control" id="exampleSelect1">
				      <option value="">1</option>
				      <option value="">2</option>
				      <option value="">3</option>
				      <option value="">4</option>
				      <option value="">5</option>
				    </select>
				  </div>
		      </td>
		      <td>
		      	<div class="form-group">
    				<input type="text" class="form-control" id="fieldID" placeholder="Field name">
  				</div>
		      </td>
		      <td>
		      	<div class="form-group">
    				<input type="text" class="form-control" id="fieldID" placeholder="Field name">
  				</div>
		      </td>
		    </tr>	
		    <tr>
		    	<td colspan="8"><button type="submit" class="btn btn-primary">Add a new line</button></td>
		    </tr>	    
		  </tbody>
		</table>

	</div>
  
	<div class="form-group">
		<label for="note">Note</label>
		<textarea class="form-control" id="note" rows="3"></textarea>
	</div>

	<br />
  
  <button type="submit" id="formSubmit" class="btn btn-primary">Submit</button>
  <button type="submit" id="formPreview" class="btn btn-primary">Preview</button>
</form>






@endsection


