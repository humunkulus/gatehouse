<!-- Stored in resources/views/child.blade.php -->

@extends('layouts.master')

@section('title', 'Invoice')

@section('sidebar')
    @parent

    <!-- <p>This is appended to the master sidebar.</p> -->
@endsection

@section('content')
	
	<div class="col-md-12">

        <div class="panel panel-default panel-table">
            <div class="panel-heading">
                <div class="row">
                  <div class="col col-xs-6">
                    <h2 class="panel-title">Invoices</h2>
                  </div>
                  <div class="col col-xs-6 text-right">
                    <a href="{{ url('invoice') }}" class="btn btn-sm btn-primary btn-create">Create New</a>
                  </div>
                </div>
              </div>
            <div class="panel-body">
			    <table class="table display" id="invoice-table">
			        <thead>
			            <tr>
			            	<th class="no-sort">#</th>
			            	<th>Invoice Number</th>
			            	<th>Reference No.</th>
			                <th>Invoice Date</th>
			                <th>Due Date</th>
			                <th>Notes</th>
			                <th>Amount</th>
			                <th>Status</th>
			                <th class="no-sort" width="170">Action</th>
			            </tr>
			        </thead>
			        <tbody>
			        	@foreach($invoice_request as $row)	
							

						    <tr> 
						    	<td></td>
						    	<td>{{$row->invoice_no}}</td>
						    	<td>{{$row->reference_no}}</td>
						    	<td>{{$row->invoice_date}}</td>
						    	<td>{{$row->due_date}}</td>
						    	<td>{{$row->notes}}</td>
						    	<td>{{$row->invoice_amount}}</td>
						    	<td>

								@if ($row->status === 1)
										Draft
									@elseif ($row->status === 2)
										For Approval
									@elseif ($row->status === 3)
										For Review
									@elseif ($row->status === 4)
										For Payment
									@else
										Paid
									@endif
						    	</td>
						    	<td>

						    		<a href="{{url('invoice-update',$row->invoice_id)}}" class="btn btn-sm btn-default" title="Update"><em class="fa fa-pencil"></em></a>
						    		
						    		<a href="{{url('invoice',$row->invoice_id)}}" class="btn btn-sm btn-primary" title="Post"><em class="fa fa-check-square"></em></a>

						    		<button type="button" data-toggle="modal" data-id="{{$row->invoice_id}}" data-title="" data-target="#preview" id="{{$row->invoice_id}}" class="btn btn-sm btn-info previewInvoice"><i class="fa fa-eye" aria-hidden="true"></i></button>

						    		<a href="#" class="btn btn-sm btn-success" title="Download"><em class="fa fa-download"></em></a>
			                        			                        
									<button type="button" data-toggle="modal" data-id="{{$row->invoice_id}}" data-title="" data-target="#delete" id="{{$row->invoice_id}}" class="btn btn-sm btn-danger deleteInvoice"><i class="fa fa-trash" aria-hidden="true"></i></button>

			                    </td>
						    </tr>
						@endforeach
			        	
			        </tbody>
			    </table>
		   </div>
	   </div>
	</div> 
@stop


@include('invoice-preview')
@include('invoice-delete')
