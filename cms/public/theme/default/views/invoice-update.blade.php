<!-- Stored in resources/views/child.blade.php -->

@extends('layouts.master')

@section('title', 'Invoice')

@section('sidebar')
    @parent

    <!-- <p>This is appended to the master sidebar.</p> -->
@endsection

@section('content')
 

	{!! Form::open(['url'=>'invoice-update',  'id'=>'invoice-update'])!!}
		
		<input type="hidden" name="_method" value="POST" />

	

	@foreach($response['header'] as $row)
		
			

	<div class="col-md-12">

        <div class="panel panel-info panel-default panel-table">
            <div class="panel-heading">
                <div class="row">
                  <div class="col col-xs-6">
                    <h3 class="panel-title">Invoices</h3>
                  </div>
                  <div class="col col-xs-6 text-right">
                    <button type="button" data-toggle="modal" data-id="" data-title="" data-target="#preview" id="formPreview" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i> Preview</button>
                  </div>
                </div>
              </div>
            <div class="panel-body">

				<div class="alert alert-danger" role="alert">
				  <strong>Error!</strong>  Change a few things up and try submitting again.
				  <p id="formError"></p>
				</div>

			    <div class="">

					<p><label>Invoice No:</label> <span id="inv_no"> {{ $row->invoice_no }} </span><p>
					<p><label>Reference: </label> <span id="inv_ref">{{ $row->reference_no }}</span><p>

					<table class="table  table-responsive">
					  <thead class="thead-inverse">
					    <tr>
					      <th>To</th>
					      <th>Date</th>
					      <th>Due Date</th>
					      <th>Amounts are</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td>
					      	<div class="">
			    				<select class="form-control" name="client" id="client">
			    					@foreach($response['client'] as $client_row)
								        <option value="{{$client_row->client_id}}">{{$client_row->name}}</option>
									@endforeach
							    </select>
			  				</div>
			  			  </td>
					      <td>
					      	<div class="">
			    				<!-- <input name="date" type="text" class="datepicker form-control" id="fieldID" placeholder="yyyy/mm/dd"> -->
			    				<div  class="input-group date" data-date-format="mm-dd-yyyy">
								    <input id="date" name="date" value="{{ $row->invoice_date }}" class="datepicker form-control" type="text" readonly />
								    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								</div> 
			  				</div>		
			  				     	
					      </td>
					      <td>
					      	<div class="">
			    				<!-- <input name="duedate" type="text" class="datepicker form-control" id="fieldID" placeholder="yyyy/mm/dd"> -->
			    				<div class="input-group date" data-date-format="mm-dd-yyyy">
								    <input id="duedate" name="duedate" value="{{ $row->due_date }}" class="datepicker form-control" type="text" readonly />
								    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								</div> 
			  				</div>
					      </td>
					      <td>
						      <div class="">					
							    <select name="tax" class="form-control compute" id="tax">
							      <option value="exclusive">Tax Exclusive</option>
							      <option value="inclusive">Tax Inclusive</option>
							      <option value="notax">No Tax</option>
							    </select>
							  </div>
						  </td>
					  </tbody>
					</table>

					<br /><br />

					<table id="invoice-detail" class="table table-bordered table-striped table-responsive">
					  <thead class="thead-inverse">
					    <tr>
					      <th>Item</th>
					      <th width="300">Description</th>
					      <th width="70">Qty</th>
					      <th>Unit Price</th>
					      <th width="70">Disc %</th>
					      <th>Tax Rate</th>
					      <th>Amount</th>
					      <th>Action</th>
					    </tr>
					  </thead>
					  <tbody>
					  	@foreach($response['details'] as $detail_row)
					    <tr>
					      <td>
					      	<div class="">
			    				<input name="item[]" value="{{$detail_row->item}}" type="text" class="item form-control" id="itemID" placeholder="Item">
			  				</div>
			  			  </td>
					      <td>
					      	<div class="">
			    				<input name="description[]" value="{{$detail_row->description}}" type="text" class="description multi form-control" id="descriptionID" placeholder="Description">
			  				</div>		      	
					      </td>
					      <td>
					      	<div class="">
			    				<input name="qty[]" type="text" value="{{$detail_row->quantity}}" class="form-control multi qty compute" id="qtyID" placeholder="Qty">
			  				</div>
					      </td>
					      <td>
					      	<div class="">
			    				<input name="price[]" type="text" value="{{$detail_row->unit_price}}" class="form-control multi price compute" id="priceID" placeholder="Unit Price">
			  				</div>
					      </td>
					      <td>
					      	<div class="">
			    				<input name="discount[]" type="text" value="{{$detail_row->discount}}" class="discount multi form-control compute" id="discountID" placeholder="%">
			  				</div>
					      </td>
					      <td>
					      	<div class="">
			    				<input name="tax-rate[]" type="text" value="{{$detail_row->tax_rate}}" class="form-control multi taxrate compute" id="taxID" placeholder="Tax Rate">
								<div id="taxrate" class="tax-class">
				    				<div>BAS Excluded</div>
									<div>GST Free Exports</div>
									<div>GST Free Income</div>
									<div>GST on Income</div>
									<div>Input Taxed</div>
								</div>
			  				</div>
					      </td>
					      <td>
					      	<div class="">
			    				<input name="amount[]" type="text" value="{{$detail_row->amount}}" disabled class="form-control multi invoice-amount" id="fieldID" placeholder="">
			  				</div>
					      </td>
					      <td>
					      	<button type="button" class="deleteRow btn btn-danger btn-sm">
					      		<i class="fa fa-fw" aria-hidden="true" title="Copy to use trash-o"></i>
					      	</button>
					      </td>
					    </tr>	

						@endforeach

					    <tr class="addRowBtn">
					    	<td colspan="8"><button type="button" class="addRow btn btn-sm btn-warning"><i class=" fa fa-plus-square-o" aria-hidden="true"></i> Add a new line</button></td>
					    </tr>
					    <tr class="custom-row">
					    	<td colspan="6" class="text-right"><label>Sub-total:</label><br /></td>
					    	<td colspan="2"><label id="sub-total">{{ $row->sub_total }}</label></td>
					    </tr>	  
					    <tr class="custom-row">
					    	<td colspan="6" class="text-right"><label>GST:</label><br /></td>
					    	<td colspan="2"><label id="gst">{{ $row->gst }}</label></td>
					    </tr>	  
					    <tr class="custom-row">
					    	<td colspan="6" class="text-right"><label>Total:</label></td>
					    	<td colspan="2"><label id="total">{{ $row->invoice_amount }}</label></td>
					    </tr>	    
					  </tbody>
					</table>

				
				</div>
			  
				<hr />


				<div class="">
					<label for="note">Note</label>
					<textarea class="form-control" name="note" id="note" rows="3"></textarea>
				</div>

				<br />
			  
			  	<div class="col col-xs-6 text-left">
			  		<button type="button" id="formSubmit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Save</button>
			  	</div>
				<div class="col col-xs-6 text-right">
                	<button type="button" id="formPost" class="btn btn-success"><i class="fa fa-check-square-o" aria-hidden="true"></i> Post</button>
                	<a href="{{ url('invoice-list') }}" id="formCancel" class="btn btn-info"><i class="fa fa-share-square-o" aria-hidden="true"></i> Cancel</a>
              	</div>
		   </div>
	   </div>
	</div> 

@endforeach

{!!Form::close() !!}

@include('invoice-preview')





@endsection


