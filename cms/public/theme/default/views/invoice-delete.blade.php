<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Delete Invoice</h4>
      </div>
      <div class="modal-body text-center">
        
        <br />
		    <label> Are you sure you want to delete this invoice? </label><br />
		

      </div>
      <div class="modal-footer">
        <div class="btn-group">
        	<button type="button" id="" class="btn btn-primary deleteThis">Yes</button>	
        	<button type="button" class="cancelDelete btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
</div>