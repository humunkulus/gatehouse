<div class="modal fade" id="preview" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog model-lg" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Invoice Preview</h4>
      </div>
      <div class="modal-body">
        

		<div class="col col-xs-6 text-left">
			<p><label>To</label> <span id="to"></span></p>
			<p><label>Invoice Number: </label> <span id="invnum">INV000001</span> </p>
			<p><label>Date Issued: </label> <span id="issued"></span> </p>
			<p><label>Due: </label> <span id="due"></span> </p>
		</div>
		<div class="col col-xs-6 text-left">
			<p><label>From</label> <span id="user">Fatima Paclibar</span> </p>
		</div>
		<br /><br />

		<table id="preview-table" class="table table-responsive table-striped table-bordered">
			<thead>
				<tr>
					<th width="350">Description</th>
					<th>Quantity</th>
					<th>Unit Price</th>
					<th>Discount</th>
					<th>GST</th>
					<th>Amount AUD</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>			
		
		<p class="text-right">Subtotal <span class="total-row" id="subtotal-row"></span></p>
		<p class="text-right">Total GST 10% <span class="total-row" id="gst-row"></span></p>
		<p class="text-right"><label>Amount Due AUD</label> <span class="total-row" id="amount-row"></span></p>
		

      </div>
      <div class="modal-footer">
        <div class="btn-group">
        	<button type="button" id="postInvoice" class="btn btn-primary">Post</button>	
        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>