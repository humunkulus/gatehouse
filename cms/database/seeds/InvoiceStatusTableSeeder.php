<?php

use Illuminate\Database\Seeder;

class InvoiceStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('invoice_status')->truncate();

        DB::table('invoice_status')->insert([
        	[
        		'name' 			=> "Draft", 
        		'description'	=> "Saves as Draft"
        	],
        	[
        		'name' 			=> "For Approval", 
        		'description'	=> "For Client Approval"
        	],
        	[
        		'name' 			=> "For Client Payment", 
        		'description'	=> "Waiting for Client's Payment"
        	],
        	[
        		'name' 			=> "For LGH Payment", 
        		'description'	=> "Waiting for LGH transfer"
        	],
        	[
        		'name' 			=> "Paid", 
        		'description'	=> "Payment Completed"
        	]
        ]);
    }
}
