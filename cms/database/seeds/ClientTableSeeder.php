<?php

use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	DB::table('client')->truncate();

     	DB::table('client')->insert([
     		[
     			'name' => "Company 1",
     			'description' => "Business Company"
     		],
     		[
     			'name' => "Company 2",
     			'description' => "Business Company"
     		],
     		[
     			'name' => "Company 2",
     			'description' => "Business Company"
     		]
     	]);
    }
}
