<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice', function(Blueprint $table){
            $table->increments('invoice_id');
            $table->string('invoice_no');
            $table->integer('client_id');
            $table->integer('freelancer_id');
            $table->integer('project_id');
            $table->date('invoice_date');
            $table->date('due_date');
            $table->string('reference_no');
            $table->float('invoice_amount', 8, 2);
            $table->text('notes');
            $table->smallInteger('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice');
    }
}
