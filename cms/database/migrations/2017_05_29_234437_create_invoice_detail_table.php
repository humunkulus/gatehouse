<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_detail', function(Blueprint $table){
            $table->increments('invoice_detail_id');
            $table->integer('item_no');
            $table->string('description');
            $table->integer('quantity');
            $table->float('unit_price', 8, 2);
            $table->date('date');
            $table->string('account_name');
            $table->float('tax_rate', 8, 2);
            $table->float('amount', 8, 2);
            $table->dateTime('created_date');
            $table->string('created_by');
            $table->dateTime('updated_date');
            $table->string('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_detail');
    }
}
