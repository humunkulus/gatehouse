<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropMultipleColumnInvoiceItem1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoice_detail', function (Blueprint $table) {
            $table->dropColumn('date', 'account_name', 'created_date', 'created_by', 'updated_date', 'updated_by', 'tax_rate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_detail', function (Blueprint $table) {
            $table->date('date');
            $table->string('account_name');
            $table->dateTime('created_date');
            $table->string('created_by');
            $table->dateTime('updated_date');
            $table->string('updated_by');
            $table->string('tax_rate', 8, 2);
        });
    }
}
