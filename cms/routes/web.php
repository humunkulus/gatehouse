<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.backend');
});

Route::get('blade', function(){
		return view('child');
});

// get client list
Route::get('invoice', 'ClientController@getClient');

// get invoice status
Route::get('/view-hr-requests', 'HrRequestController@index');

// store invoice information
//Route::post('invoice', 'InvoiceController@store');


Route::get('invoice', 'InvoiceController@showInvoiceForm')->name('invoice');
Route::post('invoice', 'InvoiceController@create');

// get invoice list
Route::get('invoice-list', 'DatatablesController@index')->name('invoice-list');
//Route::get('invoice-list', 'DatatablesController@data');




Route::get('invoice-update/{id}', 'InvoiceController@updateInvoiceView')->name('invoice-update');



//Route::resource('invoice-list', 'InvoiceController');
Route::post('invoice-list', 'InvoiceController@viewInvoice')->name('invoice-list');
//Route::post('invoice-list', 'InvoiceController@deleteInvoice')->name('invoice-list');


Route::resource('users', 'UsersController');
